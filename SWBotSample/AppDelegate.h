//
//  AppDelegate.h
//  SWBotSample
//
//  Created by Gaurav Keshre on 4/28/15.
//  Copyright (c) 2015 Softway Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

